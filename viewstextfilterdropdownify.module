<?php

/* Views Text Filter Drop Down-ify module
 * 
 * This module intercepts the specified CCK search filter fields
 * and changes them into either Autocomplete or HTML select fields
 * (depending on your choice).
 *
 * Many thanks to dboune on d.org via http://drupal.org/node/463990#comment-1616402
 * for the original code which I've only had to adjust very slightly for
 * this application!
 */

// @TODO: ensure code is tidied and to Drupal coding standards
// @TODO: add settings page a-la http://drupal.org/node/206761

function viewstextfilterdropdownify_form_alter(&$form, &$form_state, $form_id) {
  static $select_options = array();
  
  switch($form_id) {
    case 'views_exposed_form':
      // Get the list of fields we will be overriding
      $field_list = _viewstextfilterdropdownify_get_field_list($form);
      
      // Loop through each of the fields. If they're in this $form, action them
      foreach ($field_list as $field => $field_data) {
        if (is_array($form[$field]) && ($form[$field]['#type'] == 'textfield')) {
          // The field exists in this $form and it's a textfield (can't be any other type)
          // What are we going to do with this field then?
          if ($field_data['type'] == 'select') {
            // We are to convert the field to a select
            // To reduce DB load, we store this field's options in the $select_options static
            // variable. If it hasn't yet been set, we need to pull this information from the
            // db
            if (!is_array($select_options[$field])) {
              // Get the field's details from CCK
              $content_field = content_fields($field_data['name']);
              // Get the database schema information from CCK
              $db_info = content_database_info($content_field);
              // Get the range of possible options for this field
              $query = '
                SELECT
                  DISTINCT (%s)
                FROM
                  {%s}
                WHERE
                  1
                ORDER BY
                  %s ASC
              ';
              $result = db_query($query, array($db_info['columns']['value']['column'], $db_info['table'], $db_info['columns']['value']['column']));
              
              // Initialise our options
              $options = array(
                '' => '- Any -',
              );
              
              // Loop through the results from the DB adding them into the options array
              while ($row = db_fetch_array($result)) {
                // Get the result value
                $value = $row[$db_info['columns']['value']['column']];
                // If the value isn't set, append the value into the list of options
                $options[$value] = $value;
              }
              
              // Store these options into the static array. Make sure it's an array, even
              // if there are zero options, to ensure we don't unneccessarily repeat the
              // db calls
              $select_options[$field] = (array)$options;
            }
            // Set the element's options
            unset($form[$field]['#size']);
            $form[$field]['#type'] = 'select';
            $form[$field]['#options'] = $select_options[$field];
          }
          // Autocomplete Field
          elseif ($field_data['type'] == 'autocomplete') {
            $form[$field]['#autocomplete_path'] = 'viewstextfilterdropdownify/autocomplete/' . $field_data['name'];
          }
          
          $form[$field]['#default_value'] = $content_field['widget']['default_value']['value'];
     
          // Change the name of the 'Apply' button
          $form['submit']['#value'] = t('Search');
        }
      }
    break;
  }
}

function viewstextfilterdropdownify_menu() {
  return array(
    'admin/build/views/filterdropdownify' => array(
      'title' => t('Filter Dropdownify settings'),
      'description' => t('Convert boring old Views plaintext filters into select lists or autocompletes of existing values.'),
      'page callback' => 'drupal_get_form',
      'page arguments' => array('viewstextfilterdropdownify_admin'),
      'access arguments' => array('administer views'),
      'type' => MENU_LOCAL_TASK,
    ),
    'viewstextfilterdropdownify/autocomplete/%' => array(
      'title' => 'Autocomplete for Views Text Filter Dropdownify',
      'page callback' => 'viewstextfilterdropdownify_autocomplete',
      'page arguments' => array(2, 3),
      'access arguments' => array('access content'),
      'type' => MENU_CALLBACK,
    ),
  );
}

// Compile a list of the overridden fields
function _viewstextfilterdropdownify_get_field_list($form = array()) {
  // Get the overrides array
  $overrides = _viewsfiltertextdropdownify_get_overrides();
  //dpm($overrides);
  // Loop through overrides array as input_types
  $field_list = array();
  foreach ($overrides as $override_type => $overridden_fields) {
    // Loop through this input_type's overrides as CCK field names
    foreach ($overridden_fields as $overridden_field_name) {
      // If this CCK field exists as a filter in this form, get the filter name and add to the overrides $field_list array
      if (is_array($form['#info']['filter-' . $overridden_field_name . '_value'])) {
        $field_list[$form['#info']['filter-' . $overridden_field_name . '_value']['value']] = array(
          'name' => $overridden_field_name,
          'type' => $override_type,
        );
      }
    }
  }
  return $field_list;
}

function _viewstextfilterdropdownify_textarea_fields_to_array($textarea_content = '') {
  $array = (array)explode("\n", $textarea_content);
  // Useful check - make sure none of the values have _value on the end
  // and check - if any of the values DON'T have field_ at the start, show a Drupal 'warning' message
  if (!empty($array)) {
    foreach ($array as &$field) {
      $field = trim($field);
      if (!empty($field)) {
        if (substr($field, -(strlen('_value')), strlen('_value')) == '_value') {
          // They've put "_value" on the end - show a warning
          if (variable_get('viewstextfilterdropdownify_show_warnings', TRUE)) {
            drupal_set_message(
              t('Views Text Filter Dropdownify: one of your overridden fields, '
                . '!field, has "_value" on its end which might be wrong. If your '
                . 'overrides don\'t work as expected, please re-check your settings '
                . 'at !url', 
                array(
                  '!field' => $field,
                  '!url' => url('admin/build/views/filterdropdownify')
                )),
              'warning'
            );
          }
        }
        if (substr($field, 0, strlen('field_')) != 'field_') {
          // They're missing "field_" from the beginning of the field - show a warning
          if (variable_get('viewstextfilterdropdownify_show_warnings', TRUE)) {
            drupal_set_message(
              t('Views Text Filter Dropdownify: one of your overridden fields, '
                . '!field, doesn\'t start with "field". If your '
                . 'overrides don\'t work as expected, please re-check your settings '
                . 'at !url', 
                array(
                  '!field' => $field,
                  '!url' => url('admin/build/views/filterdropdownify')
                )),
              'warning'
            );
          }
        }
      }
    }
  }
  return $array;
}

function _viewsfiltertextdropdownify_get_config() {
  return array(
    'overrides' => array(
      'select' => array(
        _viewstextfilterdropdownify_textarea_fields_to_array(variable_get('viewstextfilterdropdownify_selects', '')),
      ),
      'autocomplete' => array(
        _viewstextfilterdropdownify_textarea_fields_to_array(variable_get('viewstextfilterdropdownify_autocompletes', '')),
      ),
    ),
    'options' => array(
      'results_limit' => variable_get('viewstextfilterdropdownify_autocomplete_count', 6),
      'show_warnings' => variable_get('viewstextfilterdropdownify_show_warnings', TRUE),
    ),
  );
}

function _viewsfiltertextdropdownify_get_overrides() {
  return array(
    'select' => _viewstextfilterdropdownify_textarea_fields_to_array(variable_get('viewstextfilterdropdownify_selects', array())),
    'autocomplete' => _viewstextfilterdropdownify_textarea_fields_to_array(variable_get('viewstextfilterdropdownify_autocompletes', array())),
  );
}

function viewstextfilterdropdownify_autocomplete($field, $value) {
  /*  Autocomplete function behaviour: we will return a maximum of 6-10 results at a time
      (depending on user testing and ideas theft (TM) from Google, etc.
      
      @?: If there are more than the maximum number of results, show a ... result below?
      
      Searches will be case-insensitive.
      
      @TODO: cache these results? Will be obligatory for high-traffic instances.
    */
  $results_limit = variable_get('viewstextfilterdropdownify_autocomplete_count', 6); // maximum autocomplete results
  $matches = array();
  $content_field = content_fields($field);
  $db_info = content_database_info($content_field);
  $column = $db_info['columns']['value']['column'];
  $sql = '
    SELECT
      DISTINCT (%s)
    FROM
      {%s}
    WHERE
      %s LIKE \'%%%s%%\'
    ORDER BY
      %s ASC
    LIMIT
      0, %d
  ';
  $result = db_query($sql, array($column, $db_info['table'], $column, $value, $column, $results_limit+1));
  $rows = array();
  while ($row = db_fetch_array($result)) {
    $rows[] = $row;
    // Only add this row to the results if we haven't exceeded the maximum number of results;
    // otherwise skip this step.
    if (count($rows) < ($results_limit + 1)) {
      $matches[$row[$column]] = check_plain($row[$column]);
    }
    elseif (!$dots_added) {
      // Set a flag so we don't repeat this step
      $dots_added = TRUE;
      $matches[check_plain($value)] = '...';
    }
  }
  
  drupal_json($matches);
  exit;
}

function viewstextfilterdropdownify_admin() {
  $form = array(
    'viewstextfilterdropdownify_intro' => array(
      '#type' => 'markup',
      '#value' => t('<p>This page allows you to convert plain text filters displayed by '
        . 'Views into drop-down select lists or autocomplete fields. Enter the CCK '
        . 'field names of the filters you would like to convert, one per line, in '
        . 'either the Selects or Autocompletes text boxes, below.</p>'
        . '<p>To get the names of the fields you would like to convert, go to the '
        . '!url and enter the field names, one per line, for example:</p>'
        . '!example',
        array(
          '!url' => l('CCK fields listing page', 'admin/content/types/fields'),
          '!example' => "<pre>field_country\nfield_job_title\nfield_company</pre>",
        )
      ),
    ),
    'viewstextfilterdropdownify_selects' => array(
      '#title' => t('Select lists'),
      '#description' => t('Enter one CCK field name per line here to convert to select lists. Only text filters will be converted.'),
      '#type' => 'textarea',
      '#default_value' => variable_get('viewstextfilterdropdownify_selects', ''),
    ),
    'viewstextfilterdropdownify_autocompletes' => array(
      '#title' => t('Autocomplete lists'),
      '#description' => t('Enter one CCK field name per line here to convert to autocomplete lists. Only text filters will be converted.'),
      '#type' => 'textarea',
      '#default_value' => variable_get('viewstextfilterdropdownify_autocompletes', ''),
    ),
    'viewstextfilterdropdownify_autocomplete_count' => array(
      '#title' => t('How many autocomplete results should be shown?'),
      '#description' => t('Choose how many autocomplete results you would like to show. 6 is recommended; note that the more results you show, the slower the autocomplete\'s performance will be.'),
      '#type' => 'select',
      '#default_value' => variable_get('viewstextfilterdropdownify_autocomplete_count', 6),
      '#options' => array(
        6 => '6',
        10 => '10',
        20 => '20',
      ),
    ),
    'viewstextfilterdropdownify_show_warnings' => array(
      '#title' => t('Show help tips, warnings and notices to administrators?'),
      '#description' => t('You can hide the messages shown by Filter Dropdownify, for example once you have configured this module and don\'t need to see any more routine messages. You can always re-enable them here if you wish.'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('viewstextfilterdropdownify_show_warnings', TRUE),
    ),
  );

  return system_settings_form($form);
}

/**
 * Implementation of hook_uninstall()
 */

function viewstextfilterdropdownify_uninstall() {
  variable_del('viewstextfilterdropdownify_selects');
  variable_del('viewstextfilterdropdownify_autocompletes');
  variable_del('viewstextfilterdropdownify_autocomplete_count');
  variable_del('viewstextfilterdropdownify_show_warnings');
}